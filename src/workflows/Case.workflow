<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CH_720MinutesNotification</fullName>
        <description>720 Minutes Notification tester</description>
        <protected>false</protected>
        <recipients>
            <recipient>Case Manager</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <recipient>Customer Care Manager</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>aaqib.ali@singlecrm.nokia.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>support.services@nokia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CH_Auto_Case_Handling/OK_CH_CSDNotRestoredI720Minutes</template>
    </alerts>
</Workflow>
