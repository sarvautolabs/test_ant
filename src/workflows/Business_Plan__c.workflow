<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Business_Plan_Submission_Email_Notification</fullName>
        <description>Business Plan Submission Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Partner_Program_Contact_s_PRM__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>nokia_global_partner_communications@nokia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Business_Planning/Business_Plan_Submission_Email_Notification</template>
    </alerts>
    <alerts>
        <fullName>Business_Plan_Successful_Cloning_Notification</fullName>
        <description>Business Plan Successful Cloning  Notification Description</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Partner_Program_Contact_s_PRM__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>nokia_global_partner_communications@nokia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Business_Planning/Business_Planning_Approval_Process_Notification_Ema</template>
    </alerts>
    <alerts>
        <fullName>Business_Plan_Successful_Cloning_Notification_If_not_Populate_PRM</fullName>
        <description>Business Plan Successful Cloning  Notification If not Populate PRM Description</description>
        <protected>false</protected>
        <recipients>
            <field>Nokia_Partner_Primary_Partner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>nokia_global_partner_communications@nokia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Business_Planning/Business_Planning_Successful_Cloning_Notification_For_PRM_Not_Populate</template>
    </alerts>
    <alerts>
        <fullName>Business_Plan_Successful_Cloning_Notification_PSM</fullName>
        <description>Business Plan Successful Cloning  Notification PSM Description</description>
        <protected>false</protected>
        <recipients>
            <field>Nokia_Partner_Primary_Partner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>nokia_global_partner_communications@nokia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Business_Planning/Business_Planning_Successful_Cloning_Notification_for_PSM</template>
    </alerts>
</Workflow>
