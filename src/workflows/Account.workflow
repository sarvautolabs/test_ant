<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_notification_to_be_sent_out_if_the_Partner_Status_is_Customer_Terminated_o</fullName>
        <ccEmails>gss.partnersupport@nokia.com</ccEmails>
        <description>Email notification to be sent out if Partner Status is Customer or Partner Terminated_karthik</description>
        <protected>false</protected>
        <senderAddress>nokia_global_partner_communications@nokia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Secondary_Resources/email_notification_when_a_partner_account_is_terminated</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Icare</fullName>
        <ccEmails>gss.partnersupport@nokia.com</ccEmails>
        <description>Icare Notification</description>
        <protected>false</protected>
        <senderAddress>nokia_global_partner_communications@nokia.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Secondary_Resources/Notify_Icare</template>
    </alerts>
</Workflow>
